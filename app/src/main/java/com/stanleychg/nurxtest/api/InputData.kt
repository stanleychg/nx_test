package com.stanleychg.nurxtest.api

import com.squareup.moshi.JsonClass

/**
 * @author Stanley C
 * @since 1/26/19.
 */
@JsonClass(generateAdapter = true)
class InputData(
    type: String,
    val fields: List<Field>,
    val optional: List<String>?
) : Data(type) {
    @JsonClass(generateAdapter = true)
    class Field(
        val name: String,
        val type: String
    )
}