package com.stanleychg.nurxtest.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Stanley C
 * @since 1/26/19.
 */
@JsonClass(generateAdapter = true)
class ChoiceData(
    type: String,
    val choices: List<Choice>,
    @Json(name = "max_choices") val maxChoices: Int
) : Data(type) {
    class Choice(
        val name: String,
        val value: String
    )
}