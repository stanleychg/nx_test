package com.stanleychg.nurxtest.api

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.stanleychg.nurxtest.model.NurxService
import io.reactivex.Single
import java.lang.reflect.Type


/**
 * @author Stanley C
 * @since 1/27/19.
 */
class MockNurxService : NurxService {

    val moshi: Moshi
    val jsonAdapter: JsonAdapter<List<Screen>>

    init {
        val LENIENT_FACTORY = object : JsonAdapter.Factory {
            override fun create(type: Type, annotations: Set<Annotation>, moshi: Moshi): JsonAdapter<*> {
                return moshi.nextAdapter<Any>(this, type, annotations).lenient()
            }
        }

        moshi = Moshi.Builder()
            .add(LENIENT_FACTORY)
            .add(PolymorphicJsonAdapterFactory.of(Data::class.java, "type")
                .withSubtype(InputData::class.java, "input")
                .withSubtype(ChoiceData::class.java, "choices")
                .withSubtype(RatingData::class.java, "rating"))
            .build()

        val type = Types.newParameterizedType(List::class.java, Screen::class.java)
        jsonAdapter = moshi.adapter(type)
    }

    override fun getPayload(id: String): Single<List<Screen>> = when (id) {
        "payload1.json" -> Single.fromCallable {
            jsonAdapter.fromJson(
                "[\n" +
                        "    {\n" +
                        "        \"name\": \"pii\",\n" +
                        "        \"validation\": \"function validate(firstName, lastName, email) {if (firstName === '') {return false;} if (lastName === '') {return false;} if (email === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"input\",\n" +
                        "            \"fields\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"first_name\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"last_name\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"email\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"phi\",\n" +
                        "        \"validation\": \"function validate(bloodPressure, heartRate) {if (bloodPressure === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"input\",\n" +
                        "            \"fields\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"heart_rate\",\n" +
                        "                    \"type\": \"number\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"blood pressure\",\n" +
                        "                    \"type\": \"number\"\n" +
                        "                }\n" +
                        "            ],\n" +
                        "            \"optional\": [\"heart_rate\"]\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"rating\",\n" +
                        "        \"validation\": \"function validate(selection) {if (selection === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"rating\",\n" +
                        "            \"style\": \"1to5\"  // no need to account for other scenarios\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"choices\",\n" +
                        "        \"validation\": \"function validate(selection) {if (selection.length > 1 || selection.length === 0) {return false;} for (var i = 0; i < selection.length; i++) {if (selection[i] !== 'o1' && selection[i] !== 'o2' && selection[i] !== 'o3') {return false;}} return true;}\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"choices\",\n" +
                        "            \"choices\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"option 1\",\n" +
                        "                    \"value\": \"o1\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 2\",\n" +
                        "                    \"value\": \"o2\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 3\",\n" +
                        "                    \"value\": \"o3\"\n" +
                        "                }\n" +
                        "            ],\n" +
                        "            \"max_choices\": 1\n" +
                        "        }\n" +
                        "    }\n" +
                        "]"
            )
        }
        "payload2.json" -> Single.fromCallable {
            jsonAdapter.fromJson(
                "[\n" +
                        "    {\n" +
                        "        \"name\": \"pii\",\n" +
                        "        \"validation\": \"function validate(firstName, lastName, email) {if (email === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"input\",\n" +
                        "            \"fields\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"first_name\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"last_name\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"email\",\n" +
                        "                    \"type\": \"string\"\n" +
                        "                }\n" +
                        "            ],\n" +
                        "            \"optional\": [\"first_name\", \"last_name\"]\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"phi\",\n" +
                        "        \"validation\": \"function validate(bloodPressure, heartRate) {if (bloodPressure === '') {return false;} if (heartRate === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"input\",\n" +
                        "            \"fields\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"heart_rate\",\n" +
                        "                    \"type\": \"number\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"blood pressure\",\n" +
                        "                    \"type\": \"number\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"rating\",\n" +
                        "        \"validation\": \"function validate(selection) {if (selection === '') {return false;} return true; }\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"rating\",\n" +
                        "            \"style\": \"1to5\"  // no need to account for other scenarios\n" +
                        "        }\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"name\": \"choices\",\n" +
                        "        \"optional\": \"choices\",\n" +
                        "        \"validation\": \"function validate(selection) {if (selection.length > 1 || selection.length === 0) {return false;} for (var i = 0; i < selection.length; i++) {if (selection[i] !== 'o1' && selection[i] !== 'o2' && selection[i] !== 'o3' && selection[i] !== 'o4' && selection[i] !== 'xx') {return false;}} return true;}\",\n" +
                        "        \"data\": {\n" +
                        "            \"type\": \"choices\",\n" +
                        "            \"choices\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"option 1\",\n" +
                        "                    \"value\": \"o1\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 2\",\n" +
                        "                    \"value\": \"o2\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 3\",\n" +
                        "                    \"value\": \"o3\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 4\",\n" +
                        "                    \"value\": \"o4\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"option 5\",\n" +
                        "                    \"value\": \"xx\"\n" +
                        "                }\n" +
                        "            ],\n" +
                        "            \"max_choices\": 2\n" +
                        "        }\n" +
                        "    }\n" +
                        "]"
            )
        }
        else -> Single.fromCallable { emptyList<Screen>() }
    }
}