package com.stanleychg.nurxtest.api

import android.util.Log
import org.mozilla.javascript.Context
import org.mozilla.javascript.NativeFunction
import org.mozilla.javascript.Scriptable

/**
 * @author Stanley C
 * @since 1/29/19.
 */
const val TAG = "Validation"
class Validation(private val jsValidation: String) {

    companion object {
        val NUMBER: String = "number"
        val STRING: String = "string"
    }

    fun runValidation(fieldToValue: Map<String, Any>): Boolean {
        val cx: Context = Context.enter()
        cx.optimizationLevel = -1
        try {
            val scope: Scriptable = cx.initStandardObjects()

            cx.evaluateString(scope, jsValidation, "hello", 1, null)
            val obj = scope.get("validate", scope)
            val fValidate = obj as NativeFunction
            val map = mapParamNamesToIndex(fValidate)
            val fArgs = mapDataToOrderedArgs(map, fieldToValue)

            if (map.keys.size != fArgs.size) {
                return false
            }

            return fValidate.call(cx, scope, scope, fArgs) as Boolean
        } catch (e: Exception){
            Log.e(TAG, "Error validating", e)
        } finally {
            Context.exit()
        }

        return false
    }

    private fun mapParamNamesToIndex( f: NativeFunction): Map<String, Int> {
        val arityCount : Int = f.getArity()
        val map = HashMap<String, Int>()
        val method = NativeFunction::class.java.getDeclaredMethod("getParamOrVarName", Integer.TYPE)
        method.isAccessible = true
        for (i in 0 until arityCount) {
            val paramName = method.invoke(f, i) as String
            map.put(paramName.toLowerCase(), i)
        }
        return map
    }

    private fun mapDataToOrderedArgs(paramMap: Map<String, Int>, fieldToValue: Map<String, Any>) : Array<Any> {
        val arr = Array<Any>(paramMap.size) {""}
        for ((fieldName, fieldValue) in fieldToValue) {
            val formattedFieldName = fieldName.toLowerCase().replace('_',' ').replace(" ", "")
            arr[paramMap[formattedFieldName]!!] = fieldValue
        }

        return arr
    }
}