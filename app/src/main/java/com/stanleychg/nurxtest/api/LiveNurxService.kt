package com.stanleychg.nurxtest.api

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.stanleychg.nurxtest.model.NurxService
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.lang.reflect.Type

/**
 * @author Stanley C
 * @since 1/26/19.
 */
class LiveNurxService(url: String): NurxService {

    private val api: NurxServiceApi

    private interface NurxServiceApi {
        @GET("{id}")
        fun getPayload(@Path("id") id: String): Single<List<Screen>>
    }

    init {
        val client = OkHttpClient.Builder()

        val LENIENT_FACTORY = object : JsonAdapter.Factory {
            override fun create(type: Type, annotations: Set<Annotation>, moshi: Moshi): JsonAdapter<*> {
                return moshi.nextAdapter<Any>(this, type, annotations).lenient()
            }
        }

        val moshi = Moshi.Builder()
            .add(LENIENT_FACTORY)
            .add(
                PolymorphicJsonAdapterFactory.of(Data::class.java, "type")
                    .withSubtype(InputData::class.java, "input")
                    .withSubtype(ChoiceData::class.java, "choices")
                    .withSubtype(RatingData::class.java, "rating"))
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client.build())
            .build()
        api = retrofit.create(NurxServiceApi::class.java)
    }

    override fun getPayload(id: String): Single<List<Screen>> {
        return api.getPayload(id)
    }
}
