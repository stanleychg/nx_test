package com.stanleychg.nurxtest.api

import com.squareup.moshi.JsonClass

/**
 * @author Stanley C
 * @since 1/26/19.
 */
@JsonClass(generateAdapter = true)
class Screen(
    val name: String,
    val optional: String?,
    val validation: String,
    val data: Data
) {
    fun runValidation(): Boolean {
        return false
    }
}