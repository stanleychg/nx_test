package com.stanleychg.nurxtest.api

import com.squareup.moshi.JsonClass

/**
 * @author Stanley C
 * @since 1/26/19.
 */
@JsonClass(generateAdapter = true)
class RatingData(
    type: String,
    val style: String
) : Data(type)