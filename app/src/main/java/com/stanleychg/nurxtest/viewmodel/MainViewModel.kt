package com.stanleychg.nurxtest.viewmodel

import androidx.lifecycle.ViewModel
import com.stanleychg.nurxtest.api.Screen
import com.stanleychg.nurxtest.api.Validation
import com.stanleychg.nurxtest.model.NurxRepository
import com.stanleychg.nurxtest.model.StateManager
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * @author Stanley C
 * @since 1/27/19.
 */
const val PAYLOAD_1 = "payload1.json"
const val PAYLOAD_2 = "payload2.json"
class MainViewModel : ViewModel() {

    val stateManager: StateManager = StateManager
    val nurxRepo: NurxRepository = NurxRepository

    fun refreshPayload1() : Completable {
        return nurxRepo.refreshPayload(PAYLOAD_1)
    }

    fun refreshPayload2() : Completable {
        return nurxRepo.refreshPayload(PAYLOAD_2)
    }

    fun getPayload() : Observable<List<Screen>> = nurxRepo.getPayload()
    fun saveData(screenStep: Int, fieldName: String, fieldValue: Any) {
        stateManager.mapScreenToData.getOrPut(screenStep) {HashMap()}.apply {
            put(fieldName, fieldValue)
        }
    }

    fun runValidation(screenStep: Int) : Boolean {
        val fieldNameToValue = stateManager.mapScreenToData.getOrPut(screenStep) {HashMap()}
        val currentScreen = nurxRepo.getPayload().blockingFirst()[screenStep]
        val validation = Validation(currentScreen.validation)
        return validation.runValidation(fieldNameToValue!!.toMap())
    }
}