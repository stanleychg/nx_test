package com.stanleychg.nurxtest.view.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.stanleychg.nurxtest.R
import com.stanleychg.nurxtest.api.ChoiceData

/**
 * @author Stanley C
 * @since 1/29/19.
 */
class ChoiceAdapter(val items: List<ChoiceData.Choice>) : RecyclerView.Adapter<ChoiceViewHolder>() {

    var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChoiceViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_choice, parent, false)
        return ChoiceViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getItem(pos: Int): ChoiceData.Choice {
        return items[pos]
    }

    override fun onBindViewHolder(holder: ChoiceViewHolder, position: Int) {
        tracker?.let {
            holder.bindItemToView(items.get(position), position, it.isSelected(holder.details?.selectionKey))
            return
        }

        holder.bindItemToView(items.get(position), position, false)
    }
}