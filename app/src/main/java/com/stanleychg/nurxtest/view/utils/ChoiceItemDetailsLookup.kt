package com.stanleychg.nurxtest.view.utils

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView



/**
 * @author Stanley C
 * @since 1/29/19.
 */
class ChoiceItemDetailsLookup(val recycler: RecyclerView): ItemDetailsLookup<Long>() {
    override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
        val view = recycler.findChildViewUnder(e.x, e.y)
        if (view != null) {
            val holder = recycler.getChildViewHolder(view)
            if (holder is ChoiceViewHolder) {
                return holder.details
            }
        }
        return null
    }
}