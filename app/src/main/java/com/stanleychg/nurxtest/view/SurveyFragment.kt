package com.stanleychg.nurxtest.view

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stanleychg.nurxtest.R
import com.stanleychg.nurxtest.api.ChoiceData
import com.stanleychg.nurxtest.api.Data
import com.stanleychg.nurxtest.api.InputData
import com.stanleychg.nurxtest.api.RatingData
import com.stanleychg.nurxtest.utils.toHumanReadableFormat
import com.stanleychg.nurxtest.view.utils.ChoiceAdapter
import com.stanleychg.nurxtest.view.utils.ChoiceItemDetailsLookup
import com.stanleychg.nurxtest.view.utils.ChoicePredicate
import com.stanleychg.nurxtest.viewmodel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.survey_fragment.*

/**
 * @author Stanley C
 * @since 1/27/19.
 */
const val FIELD_MARGIN = 24
const val RATING_RADIUS = 128
const val TYPE_INPUT = "input"
const val TYPE_RATING = "rating"
const val TYPE_CHOICES = "choices"

class SurveyFragment : Fragment() {

    private lateinit var toolbar: Toolbar
    private lateinit var scrollView: ViewGroup
    private lateinit var next: Button
    private lateinit var viewModel: MainViewModel

    private var selectionTracker: SelectionTracker<Long>? = null
    private var choiceAdapter: ChoiceAdapter? = null
    private var mapViewToField: MutableMap<Int, String> = HashMap()

    private lateinit var type: String
    private var step: Int = 0
    private var screenCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val arg = SurveyFragmentArgs.fromBundle(arguments)
        arg.apply {
            this@SurveyFragment.step = step
        }
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.survey_fragment, container, false)
        v.apply {
            scrollView = findViewById(R.id.survey_content)
            next = findViewById(R.id.survey_next)
        }
        toolbar = activity!!.findViewById(R.id.main_toolbar)
        return v
    }

    override fun onStart() {
        super.onStart()
        initView()
        subscribeToScreen()
    }

    private fun initView() {
        next.setOnClickListener(this::onNextClicked)
    }

    private fun onNextClicked(v: View) {
        if (type == TYPE_INPUT) {
            handleInputData()
        } else if (type == TYPE_RATING) {
            handleRatingData()
        } else if (type == TYPE_CHOICES) {
            handleChoicesData()
        }

        if (isDataValidated()) {
            if (step + 1 >= screenCount) {
                val finalAction = SurveyFragmentDirections.finalAction()
                findNavController().navigate(finalAction)
            } else {
                val nextAction = SurveyFragmentDirections.nextAction(step?.plus(1)!!)
                findNavController().navigate(nextAction)
            }
        }
    }

    private fun handleInputData() {
        for ((id, fieldName) in mapViewToField) {
            val fieldValue = this.view?.findViewById<EditText>(id)?.text.toString()
            viewModel.saveData(step, fieldName, fieldValue)
        }
    }

    private fun handleRatingData() {
        for ((id, rating) in mapViewToField) {
            val button = this.view?.findViewById<Button>(id)
            if (button!!.isSelected) {
                viewModel.saveData(step, "selection", rating)
            }
        }
    }

    private fun handleChoicesData() {
        var arr = ArrayList<String>()
        for (id in selectionTracker!!.selection) {
            arr.add(choiceAdapter!!.getItem(id.toInt()).value)
        }
        viewModel.saveData(step, "selection", arr.toArray())
    }

    private fun isDataValidated(): Boolean {
        return viewModel.runValidation(step)
    }

    private fun subscribeToScreen() {
        viewModel.getPayload()
            .doOnNext { screenCount = it.size }
            .map { it[step] }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { toolbar.title = it.name.toHumanReadableFormat() }
            .doOnNext { type = it.data.type }
            .map { it.data }
            .subscribe(this::generateView)
    }

    private fun generateView(data: Data) {
        val view = when (data) {
            is InputData -> generateFields(data)
            is RatingData -> generateRatings(data)
            is ChoiceData -> generateChoices(data)
            else -> View(context)
        }

        survey_content.addView(view)
    }

    private fun generateFields(data: InputData): View {
        var prevView: View? = null
        var contents = ConstraintLayout(context)

        contents.apply {
            id = View.generateViewId()
            layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        }

        for (field in data.fields) {
            var curView = generateFieldView(field)
            contents.addView(curView)
            val cs = ConstraintSet()
            cs.clone(contents)

            if (prevView == null) {
                cs.apply {
                    connect(
                        curView.id, ConstraintSet.TOP, contents.id, ConstraintSet.TOP,
                        FIELD_MARGIN
                    )
                    connect(
                        curView.id, ConstraintSet.LEFT, contents.id, ConstraintSet.LEFT,
                        FIELD_MARGIN
                    )
                    connect(
                        curView.id, ConstraintSet.RIGHT, contents.id, ConstraintSet.RIGHT,
                        FIELD_MARGIN
                    )
                }
            }

            prevView?.let {
                cs.apply {
                    connect(
                        curView.id, ConstraintSet.TOP, it.id, ConstraintSet.BOTTOM,
                        FIELD_MARGIN
                    )
                    connect(curView.id, ConstraintSet.LEFT, it.id, ConstraintSet.LEFT, 0)
                    connect(curView.id, ConstraintSet.RIGHT, it.id, ConstraintSet.RIGHT, 0)
                }
            }
            cs.applyTo(contents)
            prevView = curView
        }
        return contents
    }

    private fun generateFieldView(field: InputData.Field): EditText {
        val e = EditText(context)
        val viewId = View.generateViewId()
        mapViewToField.put(viewId, field.name)
        e.apply {
            layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_CONSTRAINT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            )
            id = viewId
            hint = field.name.toHumanReadableFormat()
            inputType = when (field.type) {
                "string" -> InputType.TYPE_TEXT_FLAG_CAP_WORDS
                "number" -> InputType.TYPE_CLASS_NUMBER
                else -> InputType.TYPE_TEXT_FLAG_CAP_WORDS
            }
        }

        return e
    }

    private fun generateRatings(data: RatingData): View {
        var prevView: View? = null
        var contents = ConstraintLayout(context)

        contents.apply {
            id = View.generateViewId()
            layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        }

        for (rating in 1..5) {
            var curView = generateRatingView(rating)
            contents.addView(curView)
            val cs = ConstraintSet()
            cs.clone(contents)

            if (prevView == null) {
                cs.apply {
                    connect(
                        curView.id, ConstraintSet.TOP, contents.id, ConstraintSet.TOP,
                        FIELD_MARGIN
                    )
                    connect(
                        curView.id, ConstraintSet.LEFT, contents.id, ConstraintSet.LEFT,
                        FIELD_MARGIN
                    )
                    connect(
                        curView.id, ConstraintSet.RIGHT, contents.id, ConstraintSet.RIGHT,
                        FIELD_MARGIN
                    )
                }
            }

            prevView?.let {
                cs.apply {
                    connect(
                        curView.id, ConstraintSet.TOP, it.id, ConstraintSet.BOTTOM,
                        FIELD_MARGIN
                    )
                    connect(curView.id, ConstraintSet.LEFT, it.id, ConstraintSet.LEFT, 0)
                    connect(curView.id, ConstraintSet.RIGHT, it.id, ConstraintSet.RIGHT, 0)
                }
            }
            cs.applyTo(contents)
            prevView = curView
        }
        return contents
    }

    private fun generateRatingView(rating: Int): Button {
        val b = Button(context)
        val viewId = View.generateViewId()
        b.setOnClickListener(this::onRatingClicked)
        mapViewToField.put(viewId, rating.toString())
        b.apply {
            text = rating.toString()
            layoutParams = ConstraintLayout.LayoutParams(2 * RATING_RADIUS, 2 * RATING_RADIUS)
            id = viewId
            background = resources.getDrawable(R.drawable.rating_button_selector, activity!!.theme)
        }

        return b
    }

    private fun onRatingClicked(v: View) {
        for (buttonId in mapViewToField.keys) {
            view!!.findViewById<Button>(buttonId).isSelected = false
        }
        v.isSelected = true
    }

    private fun generateChoices(data: ChoiceData): View {
        var contents = ConstraintLayout(context)

        contents.apply {
            id = View.generateViewId()
            layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        }

        val recyclerView = RecyclerView(this.context!!)
        recyclerView.apply {
            layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
            )
        }

        choiceAdapter = ChoiceAdapter(data.choices)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = choiceAdapter

        selectionTracker =  SelectionTracker.Builder<Long>(
            "choice-uri-selection",
            recyclerView,
            StableIdKeyProvider(recyclerView),
            ChoiceItemDetailsLookup(recyclerView),
            StorageStrategy.createLongStorage()
        )
            .withSelectionPredicate(ChoicePredicate(data.maxChoices))
            .build()

        choiceAdapter!!.tracker = selectionTracker

        contents.addView(recyclerView)
        return contents
    }

    override fun onStop() {
        super.onStop()
        mapViewToField.clear()
        scrollView.removeAllViews()
    }
}