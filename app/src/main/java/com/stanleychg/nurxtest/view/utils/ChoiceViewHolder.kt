package com.stanleychg.nurxtest.view.utils

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stanleychg.nurxtest.R
import com.stanleychg.nurxtest.api.ChoiceData

/**
 * @author Stanley C
 * @since 1/29/19.
 */
class ChoiceViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private lateinit var data: ChoiceData.Choice
    var details: ChoiceDetails? = null
    val textView: TextView

    init {
        textView = view.findViewById(R.id.choice_name)
    }

    fun bindItemToView(data: ChoiceData.Choice, pos: Int, isSelected: Boolean) {
        this.data = data
        details = ChoiceDetails(pos)

        textView.text = data.value

        view.isActivated = isSelected
    }
}