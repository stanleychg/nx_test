package com.stanleychg.nurxtest.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.stanleychg.nurxtest.viewmodel.MainViewModel
import com.stanleychg.nurxtest.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val TITLE = "Home"
class HomeFragment : Fragment() {

    private lateinit var toolbar: Toolbar
    private lateinit var next: Button
    private val dispose: CompositeDisposable = CompositeDisposable()
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.home_fragment, container, false)
        next = v.findViewById(R.id.home_button)
        return v
    }

    override fun onStart() {
        super.onStart()
        initView()
        subscribeToPayload()
    }

    private fun initView() {
        toolbar = activity!!.findViewById(R.id.main_toolbar)
        toolbar.title = TITLE

        val action = HomeFragmentDirections.nextAction().setStep(0)
        next.apply {
            setOnClickListener { findNavController().navigate(action) }
            isEnabled = false
        }
    }

    private fun subscribeToPayload() {
        dispose.add(viewModel.getPayload()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { next.isEnabled = true }
            .subscribe()
        )
    }

    override fun onPause() {
        super.onPause()
        unsubscribeToEverything()
    }

    private fun unsubscribeToEverything() {
        dispose.clear()
    }
}
