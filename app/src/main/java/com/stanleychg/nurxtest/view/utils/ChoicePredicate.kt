package com.stanleychg.nurxtest.view.utils

import androidx.recyclerview.selection.SelectionTracker

/**
 * @author Stanley C
 * @since 1/29/19.
 */
class ChoicePredicate(val maxSelection: Int): SelectionTracker.SelectionPredicate<Long>() {

    val map = HashSet<Long>()

    override fun canSelectMultiple(): Boolean {
        return true
    }

    override fun canSetStateForKey(key: Long, nextState: Boolean): Boolean {
        if (!nextState) {
            map.remove(key)
            return true
        }

        if (map.size >= maxSelection) {
            return false
        }

        map.add(key)
        return true
    }

    override fun canSetStateAtPosition(position: Int, nextState: Boolean): Boolean {
        return true
    }
}