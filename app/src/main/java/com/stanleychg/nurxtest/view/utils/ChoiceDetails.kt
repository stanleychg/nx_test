package com.stanleychg.nurxtest.view.utils

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup

/**
 * @author Stanley C
 * @since 1/29/19.
 */
class ChoiceDetails(val pos: Int): ItemDetailsLookup.ItemDetails<Long>() {
    override fun getSelectionKey(): Long? {
        return pos.toLong()
    }

    override fun getPosition(): Int {
        return pos
    }

    override fun inSelectionHotspot(e: MotionEvent): Boolean {
        return true
    }
}