package com.stanleychg.nurxtest

/**
 * @author Stanley C
 * @since 1/26/19.
 */
class AppConstants() {
    companion object {
        val BUILD_DEBUG: String = "debug"
        val BUILD_DEBUG_MOCK: String = "debugMock"
    }
}