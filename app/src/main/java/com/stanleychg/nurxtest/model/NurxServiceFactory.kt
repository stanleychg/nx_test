package com.stanleychg.nurxtest.model

import com.stanleychg.nurxtest.AppConstants
import com.stanleychg.nurxtest.BuildConfig
import com.stanleychg.nurxtest.api.LiveNurxService
import com.stanleychg.nurxtest.api.MockNurxService

/**
 * @author Stanley C
 * @since 1/27/19.
 */
object NurxServiceFactory {
    fun createNurxService(): NurxService = when(BuildConfig.BUILD_TYPE) {
        AppConstants.BUILD_DEBUG -> LiveNurxService(BuildConfig.Url)
        AppConstants.BUILD_DEBUG_MOCK -> MockNurxService()
        else -> LiveNurxService(BuildConfig.Url)
    }
}