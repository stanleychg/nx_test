package com.stanleychg.nurxtest.model

import com.stanleychg.nurxtest.api.Screen
import io.reactivex.Single

/**
 * @author Stanley C
 * @since 1/27/19.
 */
interface NurxService {
    fun getPayload(id: String): Single<List<Screen>>
}