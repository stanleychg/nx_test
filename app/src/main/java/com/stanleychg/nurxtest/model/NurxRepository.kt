package com.stanleychg.nurxtest.model

import com.stanleychg.nurxtest.api.Screen
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * @author Stanley C
 * @since 1/27/19.
 */
object NurxRepository {

    private val api: NurxService = NurxServiceFactory.createNurxService()
    private val latestPayload: BehaviorSubject<List<Screen>> = BehaviorSubject.create()

    fun refreshPayload(payload: String): Completable {
        return api.getPayload(payload)
            .doOnSuccess { latestPayload.onNext(it) }
            .ignoreElement()
    }

    fun getPayload(): Observable<List<Screen>> = latestPayload.hide()
}