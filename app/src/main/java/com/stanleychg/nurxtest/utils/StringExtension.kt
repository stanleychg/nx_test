package com.stanleychg.nurxtest.utils

/**
 * @author Stanley C
 * @since 1/27/19.
 */
fun String.toHumanReadableFormat(): String {
    val tokens = this.replace('_', ' ').splitToSequence(' ')
    val builder = StringBuilder()
    for (token in tokens) {
        builder.append(token.capitalize(), " ")
    }
    return builder.substring(0, builder.length-1)
}